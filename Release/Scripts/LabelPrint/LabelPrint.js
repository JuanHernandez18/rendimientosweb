﻿jQuery.noConflict();
jQuery(document).ready(function () {

    jQuery("#lstCentrosOperacion").change(function () {

        var CentrosOperacionID = jQuery("#lstCentrosOperacion").find(":selected").val();

        var values = { "CentrosOperacionID": CentrosOperacionID }

        $("#tableLabelPrint").empty()

        jQuery.ajax({
            url: "/LabelPrint/showCollaborator",
            data: JSON.stringify(values),
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                jQuery('#tableLabelPrint').html(data);
            },
            error: function () {

            }
        });
    });

    $('#SelectedAll').click(function () {
        if (!$(this).prop('checked')) {
            jQuery("#tableLabelPrint").find("input[type='checkbox']").each(function () {
                jQuery(this).prop('checked', false)

            });
        }
        else {
            jQuery("#tableLabelPrint").find("input[type='checkbox']").each(function () {
                jQuery(this).prop('checked', true)

            });
        }
    });


});

function Imprimir() {

    var ArrCodes = [];
    jQuery("#tableLabelPrint").find("input[type='checkbox']").each(function () {
        if (jQuery(this).is(':checked')) {
            var g = jQuery(this).attr('id');
            ArrCodes.push(g);
        };
    });

    var ArrNames = [];
    jQuery("#tableLabelPrint").find("input[type='checkbox']").each(function () {
        if (jQuery(this).is(':checked')) {
            var valores = $(this).parents("tr").find("td")[2].innerHTML;
            ArrNames.push(valores);
        };
    });


    var Quantity = jQuery('#LabelQuantity').val();
    var Week = jQuery('#lstWeek').find(":selected").text();
    var Printer = jQuery('#lstPrinter').find(":selected").val();


    var CentrosOperacion = jQuery("#lstCentrosOperacion").find(":selected").text();
    var Abrr = CentrosOperacion.substring(0, 1) + "-" + CentrosOperacion.substring(12, 15);

    var a = Week.replace(' ', '');
    var a = a.replace(' ', '');
    var parameters = { "Quantity": Quantity, "Week": a, "ArrayCodes": ArrCodes, "ArrNames": ArrNames, "LabelUnit": Abrr, "Printer": Printer }

    jQuery.ajax({
        url: "/LabelPrint/GenerateCode",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            console.log(data);
            location = "/LabelPrint/Download/?nombre=" + data;
        },
        error: function (xhr, textStatus, errorThrown) {
            alert(errorThrown);

            //UnlockScreen();
            //Notifications("error", errorThrown)
        },
    });
}