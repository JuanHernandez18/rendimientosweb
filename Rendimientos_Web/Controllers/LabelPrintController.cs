﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.Mvc;
using Microsoft.Win32.SafeHandles;
using Rendimientos_Web.Models;

namespace Rendimientos_Web.Controllers
{
    public class LabelPrintController : Controller
    {
        CtrlNovedadesEntities _CtrlNovedadesEntities = null;
        public ActionResult Index()
        {
            _CtrlNovedadesEntities = new CtrlNovedadesEntities();
            string currentWeek = GetWeek(DateTime.Now);

            SelectList lstCentrosOperacion = null;
            var aList = _CtrlNovedadesEntities.CentrosOperacion.Where(x => x.FlagActivo == 1 && x.Nombre.Contains("POST")).OrderBy(x => x.Nombre);
            lstCentrosOperacion = new SelectList(aList, "ID", "Nombre".Trim().ToUpper());
            ViewData["lstCentrosOperacion"] = lstCentrosOperacion;

            SelectList lstCentrosCosto = null;
            var aListc = _CtrlNovedadesEntities.CentrosCosto.Where(x => x.FlagActivo == 1).OrderBy(x => x.Nombre);
            lstCentrosCosto = new SelectList(aListc, "ID", "Nombre".Trim().ToUpper());
            ViewData["lstCentrosCosto"] = lstCentrosCosto;

            //Week
            SelectList lstWeek = null;
            var aListWeek = _CtrlNovedadesEntities.Semanas.Where(n => n.Anio >= 2019).Select(x => new { ID = x.ID, Name = x.Anio + " - " + x.NumSemana }).ToList();
            // var intSelected = aListWeek.Find(x => x.Name == currentWeek);
            lstWeek = new SelectList(aListWeek, "ID", "Name".Trim().ToUpper());
            ViewData["lstWeek"] = lstWeek;


            var lstPrinter = new List<SelectListItem>();
            lstPrinter.Add(new SelectListItem()
            {
                Text = "DATAMAX",
                Value = "1"
            });
            lstPrinter.Add(new SelectListItem()
            {
                Text = "ZEBRA",
                Value = "2"
            });

            ViewData["lstPrinter"] = lstPrinter;

            showCollaborator(lstCentrosOperacion.First().Value);

            return View();
        }

        public ActionResult showCollaborator(string CentrosOperacionID)
        {
            var a = Convert.ToInt32(CentrosOperacionID);
            _CtrlNovedadesEntities = new CtrlNovedadesEntities();

            var LabelPrint_List = _CtrlNovedadesEntities.Empleados.Where(x => x.FlagActivo == 1 && x.IDCentrosOperacion == a).OrderBy(x => x.Nombre).ToList();
            ViewBag.LabelPrint_List = LabelPrint_List;

            return PartialView("LabelPrint_List");
        }
        [DllImport("kernel32.dll", SetLastError = true)]
        static extern SafeFileHandle CreateFile(string lpFileName, FileAccess dwDesiredAccess,
        uint dwShareMode, IntPtr lpSecurityAttributes, FileMode dwCreationDisposition,
        uint dwFlagsAndAttributes, IntPtr hTemplateFile);

        public ActionResult GenerateCode(string Quantity, string Week, string[] ArrayCodes, string[] ArrNames, string LabelUnit, int Printer)
        {
            string command = "";
            if (Printer == 1) //datamax
            {
                for (int cant = 0; cant < ArrayCodes.Count(); cant++)
                {
                    int cont = 1;
                    int numero = Convert.ToInt32(ArrayCodes[cant]);

                    var Code = numero.ToString("D6");

                    command += "L D11 H15\n";
                    command += "131300000100010" + ArrNames[cant] + "\n";
                    command += "E L\n";
                    for (int total = 0; total < Convert.ToInt32(Quantity); total++)
                    {

                        if (cont == 1)
                        {
                            command += "L D11 H15\n";
                            command += "121200000300010" + Week + "\n";
                            command += "131100000100010" + LabelUnit + "\n";
                            command += "121100000000010" + Code + "\n";
                            command += "1W1D44000000000652HM," + Code + "\n";
                        }
                        if (cont == 2)
                        {
                            command += "121200000300140" + Week + "\n";
                            command += "131100000100140" + LabelUnit + "\n";
                            command += "121100000000140" + Code + "\n";
                            command += "1W1D44000000001952HM," + Code + "\n";
                        }

                        if (cont == 3)
                        {
                            cont = 0;
                            command += "121200000300270" + Week + "\n";
                            command += "131100000100270" + LabelUnit + "\n";
                            command += "121100000000270" + Code + "\n";
                            command += "1W1D44000000003252HM," + Code + "\n";

                            command += "E L\n";
                        }
                        cont++;
                    }
                    command += "E L\n";
                }
            }
            else
            {
                for (int cant = 0; cant < ArrayCodes.Count(); cant++)
                {
                    int cont = 1;
                    int numero = Convert.ToInt32(ArrayCodes[cant]);

                    var Code = numero.ToString("D6");
                    for (int total = 0; total < Convert.ToInt32(Quantity); total++)
                    {
                        if (cont == 1)
                        {
                            command += "^XA ^BY1,1,10\n";
                            command += "^FO20,6^BQN,10,4,N,N^FV" + Code + "^FS\n";
                            command += "^FO160,20^A0N50,30^FD" + Week + "^FS\n";
                            command += "^FO140,50^A0N50,30^FD" + LabelUnit + "^FS\n";
                            command += "^FO140,80^A0N50,30^FD" + Code + "^FS\n";
                        }
                        if (cont == 2)
                        {
                            command += "^FO300,6^BQN,10,4,N,N^FV" + Code + "^FS\n";
                            command += "^FO440,20^A0N50,30^FD" + Week + "^FS\n";
                            command += "^FO420,50^A0N50,30^FD" + LabelUnit + "^FS\n";
                            command += "^FO420,80^A0N50,30^FD" + Code + "^FS\n";
                        }

                        if (cont == 3)
                        {
                            cont = 0;
                            command += "^FO540,6^BQN,10,4,N,N^FV" + Code + "^FS\n";
                            command += "^FO670,20^A0N50,30^FD" + Week + "^FS\n";
                            command += "^FO650,50^A0N50,30^FD" + LabelUnit + "^FS\n";
                            command += "^FO650,80^A0N50,30^FD" + Code + "^FS\n";
                            command += "^XZ\n";
                        }
                        cont++;
                    }
                    command += "^XZ\n";
                }
            }

            var NombreArchivo = "print.txt";
            string filePath = HelperController.VerifyExportEnvironment(NombreArchivo);


            //using (StreamWriter writer = System.IO.File.CreateText(filePath))
            //{
            //    writer.WriteLine(command);
            //    writer.Close();
            //}

            StreamWriter sw = new StreamWriter(filePath);
            sw.WriteLine(command);
            sw.Close();
            return Json(NombreArchivo);

        }

        public static string GetWeek(DateTime time)
        {
            int Week = 0;

            DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
            {
                time = time.AddDays(3);
            }

            Week = CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

            return time.Year.ToString("0000") + "-" + Week.ToString();
        }

        public ActionResult Download(string nombre)
        {
            string filePath = HelperController.VerifyExportEnvironment("");
            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath + nombre);
            System.IO.File.Delete(filePath + nombre);
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, nombre);
        }
    }
}