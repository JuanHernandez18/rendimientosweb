﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Rendimientos_Web.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class BASEntities : DbContext
    {
        public BASEntities()
            : base("name=BASEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<cre> BusinessUnit { get; set; }
        public virtual DbSet<BouquetType> BouquetType { get; set; }
    
        public virtual ObjectResult<sp_GetPerformance_Result> sp_GetPerformance(Nullable<int> businessUnitID, Nullable<System.DateTime> date)
        {
            var businessUnitIDParameter = businessUnitID.HasValue ?
                new ObjectParameter("BusinessUnitID", businessUnitID) :
                new ObjectParameter("BusinessUnitID", typeof(int));
    
            var dateParameter = date.HasValue ?
                new ObjectParameter("Date", date) :
                new ObjectParameter("Date", typeof(System.DateTime));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_GetPerformance_Result>("sp_GetPerformance", businessUnitIDParameter, dateParameter);
        }
    
        public virtual ObjectResult<sp_GetPerformanceSummary_Result> sp_GetPerformanceSummary(Nullable<int> businessUnitID, Nullable<System.DateTime> date)
        {
            var businessUnitIDParameter = businessUnitID.HasValue ?
                new ObjectParameter("BusinessUnitID", businessUnitID) :
                new ObjectParameter("BusinessUnitID", typeof(int));
    
            var dateParameter = date.HasValue ?
                new ObjectParameter("Date", date) :
                new ObjectParameter("Date", typeof(System.DateTime));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_GetPerformanceSummary_Result>("sp_GetPerformanceSummary", businessUnitIDParameter, dateParameter);
        }
    }
}
